<?php namespace Ghost\Seo\Components;

use Cms\Classes\ComponentBase;
use Ghost\Seo\Models\Rule;
use October\Rain\Parse\Twig;

class Seo extends ComponentBase
{

    private $_rule;

    public function componentDetails()
    {
        return [
            'name'        => 'Seo',
            'description' => 'Seo plugin'
        ];
    }

    public function onRender()
    {
        if (!$this->_getNativeRuleByUri()){
            $this->_getGroupRuleByUriMask();
        }

        if ($this->_rule) {

            $variables = $this->_getMaskVariables($this->_rule->title, $this->_rule->keywords, $this->_rule->description);

            $variablesForRendering = $this->_getVariablesForRender($variables);

            $this->page->seo_title              = (new Twig())->parse($this->_rule->title, $variablesForRendering);
            $this->page->seo_meta_keywords      = (new Twig())->parse($this->_rule->keywords, $variablesForRendering);
            $this->page->seo_meta_description   = (new Twig())->parse($this->_rule->description, $variablesForRendering);
        }
    }

    /**
     * Search native url in rules
     */
    private function _getNativeRuleByUri()
    {
        return $this->_rule = Rule::query()
            ->byUri(request()->path())
            ->get()
            ->first();
    }

    /**
     * Search native url in rules
     */
    private function _getGroupRuleByUriMask()
    {
        $rules = Rule::all()->filter(function($rule) {
            return request()->is($rule->rule);
        });

        $this->_rule = $rules->where('rule', $rules->max('rule'))
                             ->first();
    }

    /**
     * Получаем все названия переменных
     * в тексте заданных маской
     * @param string ...$vars
     * @return array
     */
    private function _getMaskVariables(string ...$vars)
    {
        $matches = [];
        preg_match_all('/{{ .*? }}/', implode(' ', $vars), $matches);

        $matches = array_map(function ($e) {
            return trim(str_replace(['{{', '}}'], '', $e));
        }, array_first($matches));

        return $matches;
    }

    /**
     * @param array $vars
     * @return array
     */
    private function _getVariablesForRender(array $vars)
    {
        $variables = [];

        foreach ($vars as $var) {
            if ($this->property($var)) {
                $variables[$var] = $this->property($var);
            }
        }

        return $variables;
    }

}
