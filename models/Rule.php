<?php namespace Ghost\Seo\Models;

use Ghost\Review\Models\Settings;
use Model;
use System\Classes\PluginManager;
use Storage;
use Str;
/**
 * Model
 */
class Rule extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ghost_seo_rules';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


    /**
     * @param $query
     * @param $uri
     */
    public function scopeByUri($query, $uri)
    {
        $query->whereRule($uri);
    }


}
