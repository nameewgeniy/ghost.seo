<?php namespace Ghost\Seo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGhostSeoRules extends Migration
{
    public function up()
    {
        Schema::create('ghost_seo_rules', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('rule');
            $table->text('title');
            $table->text('keywords')->nullable();
            $table->text('description')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ghost_seo_rules');
    }
}
