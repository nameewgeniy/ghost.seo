<?php namespace Ghost\Seo;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ghost\Seo\Components\Seo' => 'seo'
        ];
    }


}
